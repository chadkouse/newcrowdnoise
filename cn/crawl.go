package cn

import (
	"appengine"
	"appengine/taskqueue"
	"appengine/urlfetch"
	"fmt"
	"net/http"
	"strings"
	"time"
)

//func main() {
//
//	var teams = flag.Bool("teams", false, "Crawl teams for given league")
//	var schedule = flag.Bool("sched", false, "Crawl schedule for given league")
//	var subreddits = flag.Bool("subreddits", false, "Find subreddits for each team")
//	var debug = flag.Bool("debug", false, "Enable debugging, only output instead of writing to DB")
//	flag.Parse()
//
//	if flag.NArg() == 0 {
//		flag.PrintDefaults()
//		return
//	}
//
//	var league = flag.Arg(flag.NArg() - 1)
//
//	if len(league) <= 0 {
//		flag.PrintDefaults()
//		return
//	}
//
//	var err error
//	if teams != nil && *teams {
//		err = crawlTeams(league, *debug)
//	} else if schedule != nil && *schedule {
//		err = crawlSched(league, *debug)
//	} else if subreddits != nil && *subreddits {
//		crawlSubreddits(league, *debug)
//	} else {
//		flag.PrintDefaults()
//		return
//	}
//
//	if err != nil {
//		c.Infof("Error: ", err)
//	}
//	/*
//		ts := &fetch.TheScoreFetcher{}
//
//		league := model.League{}
//		league.ShortName = "nfl"
//		_, err := ts.FetchEvents(league, time.Now().AddDate(0, 0, -1), time.Now().AddDate(0, 0, 10))
//
//		if err != nil {
//			c.Infof("Error", err)
//		}
//
//	*/
//}

func CrawlAllLeagueTeams(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)

	_, leagues, err := GetAllLeagues(c, "", 100)

	for i := range leagues {
		l := leagues[i]

		c.Infof("Adding task for %v", l.Name)
		t := &taskqueue.Task{
			Path:   "/tasks/crawl/subreddits?league=" + l.Name,
			Method: "GET",
		}

		_, err = taskqueue.Add(c, t, "")
		if err == taskqueue.ErrTaskAlreadyAdded {
			err = nil
		}
		if err != nil {
			http.Error(w, "Error importing", http.StatusInternalServerError)
		}

	}

}

func CrawlTeams(w http.ResponseWriter, r *http.Request) {

	c := appengine.NewContext(r)
	leagueName := r.FormValue("league")

	c.Infof("Crawling teams for %v", leagueName)

	l, err := GetLeagueByName(c, leagueName)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	} else if l.Id <= 0 {
		http.Error(w, fmt.Sprintf("No league found for %v", leagueName), http.StatusInternalServerError)
		return
	}

	ts := NewSportsFetcher(urlfetch.Client(c))
	teams, err := ts.FetchTeams(c, l)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	ins := 0

	for _, t := range teams {
		existing, _ := GetTeamByName(c, t.Name)
		if existing.Id <= 0 {
			_, err := t.Create(c)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
			}
			ins++
		}
	}

	c.Infof("Found", len(teams), "teams, inserted", ins, "rows")
}

func CrawlSched(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	leagueName := r.FormValue("league")

	c.Infof("Crawling sched for", leagueName)

	l, err := GetLeagueByName(c, leagueName)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	} else if l.Id <= 0 {
		http.Error(w, fmt.Sprintf("No league found for %v", leagueName), http.StatusInternalServerError)
		return
	}

	ts := NewSportsFetcher(urlfetch.Client(c))
	events, err := ts.FetchEvents(c, l, time.Now().AddDate(0, 0, -7), time.Now().AddDate(1, 0, 0))

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	ins := 0
	for _, e := range events {
		// check for existing
		_, events, err := FindEventsAtTime(c, e.HomeTeamId, "", 1000, e.StartTime, e.EndTime)
		if err == nil && len(events) == 0 {
			_, err := e.Create(c)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
			}
			ins++
		}
	}

	c.Infof("Found", len(events), "events, inserted", ins, "rows")

}

func CrawlSubreddits(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	rc := GetRedditClient()

	leagueName := r.FormValue("league")

	l, err := GetLeagueByName(c, leagueName)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	} else if l.Id <= 0 {
		http.Error(w, fmt.Sprintf("No league found for %v", leagueName), http.StatusInternalServerError)
		return
	}

	_, teams, err := GetAllTeamsByLeagueId(c, l.Id, "", 1000)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	missing := 0
	updates := 0

	for _, t := range teams {
		if len(t.Subreddit) > 0 {
			continue
		}

		missing++

		res, err := rc.SearchSubreddits(t.Name, 1)
		if err != nil {
			c.Infof("Subreddit search failed", err)
			continue
		} else if len(res) != 1 {
			c.Infof("No subreddits found for team", t.Name)
			continue
		}

		if res[0].Data.Subscribers > 1000 {
			bare := strings.TrimPrefix(res[0].Data.Url, "/r/")
			bare = strings.TrimSuffix(bare, "/")
			t.Subreddit = bare
			_, err := t.Update(c)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
			}
			updates++
		} else {
			c.Infof("Ignoring", res[0].Data.Url, "due to only", res[0].Data.Subscribers, "subs")
		}
	}

	c.Infof("Found %d teams. %d missing subreddits. Updated %d records", len(teams), missing, updates)
}
