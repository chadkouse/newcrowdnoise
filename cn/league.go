package cn

import (
	"appengine"
	"appengine/datastore"
	"errors"
	"github.com/chadkouse/hal"
)

var (
	ErrNoSuchLeague = errors.New("person not found")
)

type League struct {
	Id           int64  `json:"id", datastore:"-"`
	Name         string `json:"name"`
	Subreddit    string `json:"subreddit"`
	ShortName    string `json:"shortName"`
	IconChar     int    `json:"iconChar"`
	IconColor    string `json:"iconColor"`
	GameDuration int    `json:"-"`
}

func (l League) GetMap() hal.Entry {
	return hal.Entry{
		"id":        l.Id,
		"name":      l.Name,
		"subreddit": l.Subreddit,
		"shortName": l.ShortName,
		"iconChar":  l.IconChar,
		"iconColor": l.IconColor,
	}
}

func (p *League) GetKey(c appengine.Context) *datastore.Key {
	return datastore.NewKey(c, "League", "", p.Id, nil)
}

func (p *League) Create(c appengine.Context) (*datastore.Key, error) {

	k := datastore.NewIncompleteKey(c, "League", nil)

	newKey, err := datastore.Put(c, k, p)
	p.Id = newKey.IntID()

	return newKey, err
}

func (p *League) Get(c appengine.Context) error {

	k := datastore.NewKey(c, "League", "", p.Id, nil)

	err := datastore.Get(c, k, &p)
	if err == datastore.ErrNoSuchEntity {
		err = ErrNoSuchLeague
	}

	if err != nil {
		return err
	}
	p.Id = k.IntID()

	return nil

}

func (p *League) Update(c appengine.Context) (*datastore.Key, error) {

	k := p.GetKey(c)

	return datastore.Put(c, k, p)

}

func (p *League) Delete(c appengine.Context) error {

	k := p.GetKey(c)
	return datastore.Delete(c, k)
}

func GetAllLeagues(c appengine.Context, startKey string, limit int) (string, []League, error) {

	q := datastore.NewQuery("League")

	cursor, err := datastore.DecodeCursor(startKey)
	if err == nil {
		q = q.Start(cursor)
	}

	// Iterate over the results.
	leagues := []League{}

	if limit > 0 {
		q = q.Limit(limit)
	}

	t := q.Run(c)
	for {
		l := League{}
		k, err := t.Next(&l)
		if err == datastore.Done {
			break
		}

		if err != nil {
			continue
		}

		l.Id = k.IntID()
		leagues = append(leagues, l)

	}

	// Get updated cursor and store it for next time.
	if cursor, err := t.Cursor(); err == nil {
		return cursor.String(), leagues, err
	}

	return "", leagues, err

}

func GetAllTeamsByLeagueId(c appengine.Context, leagueId int64, startKey string, limit int) (string, []Team, error) {

	pk := datastore.NewKey(c, "League", "", leagueId, nil)
	q := datastore.NewQuery("Team").Ancestor(pk)

	cursor, err := datastore.DecodeCursor(startKey)
	if err == nil {
		q = q.Start(cursor)
	}

	// Iterate over the results.
	teams := []Team{}

	q = q.Limit(limit)

	t := q.Run(c)
	for {
		var newTeam Team
		k, err := t.Next(&newTeam)
		if err == datastore.Done {
			break
		}
		if err != nil {
			c.Errorf("fetching next Team: %v", err)
			break
		}

		newTeam.Id = k.IntID()
		teams = append(teams, newTeam)
	}

	// Get updated cursor and store it for next time.
	if cursor, err := t.Cursor(); err == nil {
		return cursor.String(), teams, err
	}

	return "", teams, err

}

func GetLeagueByName(c appengine.Context, name string) (League, error) {

	q := datastore.NewQuery("League").Filter("Name =", name)

	// Iterate over the results.
	leagues := []League{}
	league := League{}

	keys, err := q.GetAll(c, &leagues)
	if err != nil {
		return league, err
	}

	for i := range keys {
		leagues[i].Id = keys[i].IntID()
	}

	if len(leagues) > 0 {
		league = leagues[0]
	}
	return league, nil

}
