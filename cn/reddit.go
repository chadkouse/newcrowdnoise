package cn

import (
	"errors"
	"fmt"
	"net/http"
	"regexp"
	"strings"

	"appengine"
	"github.com/hobbs/GoReddit"
)

type redditFetcher struct {
	cachedListings map[string][]goreddit.Link
}

func (r *redditFetcher) FetchTeamChat(client *http.Client, team Team, event Event) (Chat, error) {

	rc := GetRedditClient()
	rc.HttpClient = client

	listing, err := rc.GetSubreddit(team.Subreddit, "hot", 100)

	if err != nil {
		return Chat{}, err
	}

	for _, link := range listing {
		if r.isGameThread(link.Data.Title) || r.isGameThread(link.Data.Link_flair_text) {
			chat := Chat{}
			chat.EventId = event.Id
			chat.Name = team.Name + " game thread"
			chat.Providor = "reddit"
			chat.Value = link.Data.Id
			chat.TeamId = team.Id
			return chat, nil
		}
	}

	return Chat{}, errors.New("No thread found")
}

func (r *redditFetcher) FetchLeagueChat(ctx appengine.Context, client *http.Client, league League, event Event) (Chat, error) {
	rc := GetRedditClient()
	rc.HttpClient = client

	var listing []goreddit.Link
	if val, ok := r.cachedListings[league.Subreddit]; ok {
		listing = val
		fmt.Println("Using cache")

	} else {
		var err error
		listing, err = rc.GetSubreddit(league.Subreddit, "hot", 100)
		fmt.Println("Using network")

		if err != nil {
			return Chat{}, err
		}

		if len(listing) > 0 {
			r.cachedListings[league.Subreddit] = listing
		}
	}

	home := Team{Id: event.HomeTeamId}
	err := home.Get(ctx)

	if err != nil {
		return Chat{}, err
	}

	away := Team{Id: event.AwayTeamId}
	err = away.Get(ctx)
	if err != nil {
		return Chat{}, err
	}

	for _, link := range listing {
		if r.isGameThread(link.Data.Title) {

			if r.isRelatedToTeam(link.Data.Title, home) && r.isRelatedToTeam(link.Data.Title, away) {
				chat := Chat{}
				chat.EventId = event.Id
				chat.Name = league.Name + " game thread"
				chat.Providor = "reddit"
				chat.Value = link.Data.Id
				return chat, nil
			}
		}
	}

	return Chat{}, errors.New("No thread found")
}

func (r *redditFetcher) isGameThread(title string) bool {
	matched, _ := regexp.MatchString("(?i)gameday thread|game thread|game day thread|game thread|game chat", title)
	return matched
}

func (r *redditFetcher) isRelatedToTeam(title string, team Team) bool {
	orStr := strings.Replace(team.Name, " ", "|", -1)
	matched, _ := regexp.MatchString("(?i)"+orStr, title)
	return matched
}
