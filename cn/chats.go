package cn

import (
	"net/http"

	"appengine"
	"github.com/hobbs/GoReddit"
)

type ChatFetcher interface {
	FetchTeamChat(*http.Client, Team, Event) (Chat, error)
	FetchLeagueChat(ctx appengine.Context, client *http.Client, league League, event Event) (Chat, error)
}

func NewRedditFetcher() ChatFetcher {
	r := &redditFetcher{}
	r.cachedListings = make(map[string][]goreddit.Link)
	return r
}
