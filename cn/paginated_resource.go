package cn

import (
	"fmt"
	"github.com/chadkouse/hal"
)

type PaginatedResource struct {
	SelfBase     string
	StartKey     string
	PrevStartKey string
	NextStartKey string
	Limit        int
}

func (p PaginatedResource) GetMap() hal.Entry {
	return hal.Entry{}
}

func (p PaginatedResource) GetResource() *hal.Resource {
	r := hal.NewResource(p, fmt.Sprintf("%v?startKey=%v&prevStartKey=%v&limit=%v", p.SelfBase, p.StartKey, p.PrevStartKey, p.Limit))
	r.AddNewLink("next", fmt.Sprintf("%v?startKey=%v&prevStartKey=%v&limit=%v", p.SelfBase, p.NextStartKey, p.StartKey, p.Limit))

	if p.StartKey != "" {
		r.AddNewLink("prev", fmt.Sprintf("%v?startKey=%v&limit=%v", p.SelfBase, p.PrevStartKey, p.Limit))
	}

	return r
}
