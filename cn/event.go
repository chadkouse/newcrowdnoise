package cn

import (
	"appengine"
	"appengine/datastore"
	"errors"
	"github.com/chadkouse/hal"
	"sort"
	"time"
)

var (
	ErrNoSuchEvent = errors.New("person not found")
)

type Event struct {
	Id int64 `json:"id", datastore:"-"`

	LeagueId   int64 `json:"leagueId"`
	HomeTeamId int64 `json:"homeTeamId"`
	AwayTeamId int64 `json:"awayTeamId"`

	StartTime int64 `json:"startTime"`
	EndTime   int64 `json:"endTime"`
}

func (e Event) GetMap() hal.Entry {
	return hal.Entry{
		"id":         e.Id,
		"leagueId":   e.LeagueId,
		"homeTeamId": e.HomeTeamId,
		"awayTeam":   e.AwayTeamId,
		"startTime":  e.StartTime,
		"endTime":    e.EndTime,
	}
}

func (p *Event) GetKey(c appengine.Context) *datastore.Key {
	return datastore.NewKey(c, "Event", "", p.Id, nil)
}

func (p *Event) Create(c appengine.Context) (*datastore.Key, error) {

	k := datastore.NewIncompleteKey(c, "Event", nil)

	newKey, err := datastore.Put(c, k, p)
	p.Id = newKey.IntID()

	return newKey, err
}

func (p *Event) Get(c appengine.Context) error {

	k := p.GetKey(c)

	err := datastore.Get(c, k, &p)
	if err == datastore.ErrNoSuchEntity {
		err = ErrNoSuchEvent
	}

	if err != nil {
		return err
	}
	p.Id = k.IntID()

	return nil

}

func (p *Event) Update(c appengine.Context) (*datastore.Key, error) {

	k := p.GetKey(c)

	return datastore.Put(c, k, p)

}

func (p *Event) Delete(c appengine.Context) error {
	k := p.GetKey(c)
	return datastore.Delete(c, k)
}

func GetAllEvents(c appengine.Context, startKey string, limit int) (string, []Event, error) {

	q := datastore.NewQuery("Event")

	cursor, err := datastore.DecodeCursor(startKey)
	if err == nil {
		q = q.Start(cursor)
	}

	// Iterate over the results.
	events := []Event{}

	if limit > 0 {
		q = q.Limit(limit)
	}

	t := q.Run(c)
	for {
		e := Event{}
		key, err := t.Next(&e)
		if err == datastore.Done {
			break
		}

		if err != nil {
			continue
		}

		e.Id = key.IntID()

		events = append(events, e)
	}

	// Get updated cursor and store it for next time.
	if cursor, err := t.Cursor(); err == nil {
		return cursor.String(), events, err
	}

	return "", events, err

}

type ByStartTime []Event

func (a ByStartTime) Len() int           { return len(a) }
func (a ByStartTime) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByStartTime) Less(i, j int) bool { return a[i].StartTime < a[j].StartTime }

type ByLeagueId []Event

func (a ByLeagueId) Len() int           { return len(a) }
func (a ByLeagueId) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByLeagueId) Less(i, j int) bool { return a[i].LeagueId < a[j].LeagueId }

// Get all events in progress or starting in the next 'secs' seconds
func GetUpcomingEvents(c appengine.Context, secs int64) ([]Event, error) {

	now := int32(time.Now().Unix())
	lagSeconds := int32(time.Hour / time.Second) //(seconds in an hour)

	q := datastore.NewQuery("Event").
		Filter("EndTime >", now-lagSeconds)

	// Iterate over the results.
	events := []Event{}

	_ = q.Run(c)
	keys, err := q.GetAll(c, &events)
	if err != nil {
		return events, err
	}

	upcomingEvents := []Event{}

	for i := range keys {
		events[i].Id = keys[i].IntID()
		if events[i].StartTime < int64(now+lagSeconds) {
			upcomingEvents = append(upcomingEvents, events[i])
		}
	}
	sort.Sort(ByStartTime(upcomingEvents))
	sort.Sort(ByLeagueId(upcomingEvents))

	return upcomingEvents, nil
}

func FindEventsAtTime(c appengine.Context, teamId int64, startKey string, limit int, start int64, finish int64) (string, []Event, error) {
	q := datastore.NewQuery("Event").
		Filter("HomeTeamId =", teamId).
		Filter("StartTime >=", start).
		Filter("EndTime <=", finish)

	cursor, err := datastore.DecodeCursor(startKey)
	if err == nil {
		q = q.Start(cursor)
	}

	// Iterate over the results.
	events := []Event{}

	q = q.Limit(limit)

	t := q.Run(c)
	keys, err := q.GetAll(c, &events)
	if err != nil {
		return "", events, err
	}

	for i := range keys {
		events[i].Id = keys[i].IntID()
	}

	// Get updated cursor and store it for next time.
	if cursor, err := t.Cursor(); err == nil {
		return cursor.String(), events, err
	}

	return "", events, err
}
