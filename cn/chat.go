package cn

import (
	"appengine"
	"appengine/datastore"
	"errors"
	"github.com/chadkouse/hal"
)

var (
	ErrNoSuchChat = errors.New("person not found")
)

type Chat struct {
	Id       int64  `json:"id", datastore:"id"`
	Name     string `json:"name"`
	Value    string `json:"value"`
	Providor string `json:"provider"`
	EventId  int64  `json:"eventId"`
	TeamId   int64  `json:"teamId"`
}

func (c Chat) GetMap() hal.Entry {
	return hal.Entry{
		"id":       c.Id,
		"name":     c.Name,
		"value":    c.Value,
		"provider": c.Providor,
		"eventId":  c.EventId,
		"teamId":   c.TeamId,
	}
}

func (p *Chat) GetKey(c appengine.Context) *datastore.Key {
	return datastore.NewKey(c, "Chat", "", p.Id, nil)
}

func (p *Chat) Create(c appengine.Context) (*datastore.Key, error) {

	k := datastore.NewIncompleteKey(c, "Chat", nil)

	newKey, err := datastore.Put(c, k, p)
	p.Id = newKey.IntID()

	return newKey, err
}

func (p *Chat) Get(c appengine.Context) error {

	k := p.GetKey(c)

	err := datastore.Get(c, k, p)
	if err == datastore.ErrNoSuchEntity {
		err = ErrNoSuchChat
	}

	if err != nil {
		return err
	}
	p.Id = k.IntID()

	return nil

}

func (p *Chat) Update(c appengine.Context) (*datastore.Key, error) {

	k := p.GetKey(c)

	return datastore.Put(c, k, p)

}

func (p *Chat) Delete(c appengine.Context) error {
	k := p.GetKey(c)
	return datastore.Delete(c, k)
}

func GetAllChats(c appengine.Context, startKey string, limit int) (string, []Chat, error) {

	q := datastore.NewQuery("Chat")

	cursor, err := datastore.DecodeCursor(startKey)
	if err == nil {
		q = q.Start(cursor)
	}

	// Iterate over the results.
	chats := []Chat{}

	if limit > 0 {
		q = q.Limit(limit)
	}

	t := q.Run(c)
	for {
		c := Chat{}
		key, err := t.Next(&c)
		if err == datastore.Done {
			break
		}

		if err != nil {
			continue
		}
		c.Id = key.IntID()
		chats = append(chats, c)
	}

	// Get updated cursor and store it for next time.
	if cursor, err := t.Cursor(); err == nil {
		return cursor.String(), chats, err
	}

	return "", chats, err

}

func GetChatsForEvent(c appengine.Context, event_id int64) ([]Chat, error) {

	q := datastore.NewQuery("Chat").Filter("EventId =", event_id)

	// Iterate over the results.
	chats := []Chat{}

	keys, err := q.GetAll(c, &chats)
	if err != nil {
		return chats, err
	}

	for i := range keys {
		chats[i].Id = keys[i].IntID()
	}

	return chats, nil

}
