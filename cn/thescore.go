package cn

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math"
	"strconv"
	"strings"
	"time"

	"appengine"
	"github.com/cznic/mathutil"
	"net/http"
)

type tsScheduleJSON struct {
	CurrentSeason []tsEventGroup `json:"current_season"`
	CurrentGroup  tsEventGroup   `json:"current_group"`
}

type tsEventGroup struct {
	Guid       string
	Id         string
	Label      string
	SeasonType string    `json:"season_type"`
	StartDate  time.Time `json:"start_date"`
	EndDate    time.Time `json:"end_date"`
	EventIds   []int     `json:"event_ids"`
}

type tsEvent struct {
	GameDate string `json:"game_date"`
	AwayTeam tsTeam `json:"away_team"`
	HomeTeam tsTeam `json:"home_team"`
}

type tsTeam struct {
	FullName string `json:"full_name"`
	Color1   string `json:"colour_1"`
	Color2   string `json:"colour_2"`
}

const (
	tsApi         = "http://api.thescore.com/"
	tsEventsBatch = 20
)

type TheScoreFetcher struct {
	client *http.Client
}

func (ts *TheScoreFetcher) SetClient(client *http.Client) {
	ts.client = client
}

func (ts *TheScoreFetcher) GetClient() *http.Client {
	return ts.client
}

func (ts *TheScoreFetcher) getJSON(path string) ([]byte, error) {
	url := tsApi + path
	result, err := ts.client.Get(url)

	if err != nil {
		return nil, err
	}

	defer result.Body.Close()

	return ioutil.ReadAll(result.Body)
}

func (ts *TheScoreFetcher) FetchEvents(ctx appengine.Context, league League, start time.Time, end time.Time) ([]Event, error) {

	response, err := ts.getJSON(league.ShortName + "/schedule")
	if err != nil {
		return nil, err
	}

	sched := tsScheduleJSON{}
	err = json.Unmarshal(response, &sched)
	if err != nil {
		return nil, err
	}

	var events []int

	// Get all event ids in date range
	for _, v := range sched.CurrentSeason {
		if v.StartDate.After(start) && v.StartDate.Before(end) {
			events = append(events, v.EventIds...)
		}
	}

	//fmt.Printf("Found %d events in range\n", len(events))

	// Grab event details for all events
	n := int(math.Ceil(float64(len(events)) / float64(tsEventsBatch)))
	c := make(chan []tsEvent)

	for i := 0; i < n; i++ {
		i1 := i * tsEventsBatch
		i2 := mathutil.MinInt64(int64((i+1)*tsEventsBatch), int64(len(events)-1))
		//fmt.Printf("Processing event %d - %d\n", i1, i2)
		go ts.getEvent(league, events[i1:i2], c)
	}

	var detailed []tsEvent

	for i := 0; i < n; i++ {
		e := <-c
		if e != nil {
			detailed = append(detailed, e...)
			//fmt.Printf("Found %d more events\n", len(e))
		}
	}

	//fmt.Printf("Found a total of %d events \n", len(detailed))

	// For each event, find team id and build Event struct
	var ret []Event
	for _, v := range detailed {
		e := Event{}
		e.LeagueId = league.Id

		home, err := GetTeamByName(ctx, v.HomeTeam.FullName)
		if err != nil || home.Id == 0 {
			fmt.Printf("Couldn't find team '%s'\n", v.HomeTeam.FullName)
			continue
		} else {
			e.HomeTeamId = home.Id
		}

		away, err := GetTeamByName(ctx, v.AwayTeam.FullName)
		if err != nil || away.Id == 0 {
			fmt.Printf("Couldn't find team '%s'\n", v.AwayTeam.FullName)
			continue
		} else {
			e.AwayTeamId = away.Id
		}

		t, err := time.Parse(time.RFC1123Z, v.GameDate)
		if err == nil {
			e.StartTime = t.UTC().Unix()
			e.EndTime = t.Add(time.Duration(league.GameDuration) * time.Second).UTC().Unix()
		}

		ret = append(ret, e)
	}

	return ret, nil
}

func (ts *TheScoreFetcher) getEvent(league League, ids []int, c chan []tsEvent) {

	var idList []string
	for _, v := range ids {
		idList = append(idList, strconv.Itoa(v))
	}

	path := league.ShortName + "/events?id.in=" + strings.Join(idList, ",")
	fmt.Println("Making API call: ", path)
	response, err := ts.getJSON(path)
	if err != nil {
		c <- nil
		fmt.Println("Got error ", err)
		return
	}

	var events []tsEvent
	err = json.Unmarshal(response, &events)
	if err != nil {
		c <- nil
		fmt.Println("Got error ", err)
		return
	}
	c <- events
}

func (ts *TheScoreFetcher) FetchTeams(c appengine.Context, league League) ([]Team, error) {
	response, err := ts.getJSON(league.ShortName + "/teams")
	if err != nil {
		return nil, err
	}

	var teams []tsTeam
	err = json.Unmarshal(response, &teams)
	if err != nil {
		return nil, err
	}

	var ret []Team

	// Get all event ids in date range
	for _, v := range teams {
		t := Team{}
		t.Name = v.FullName
		t.LeagueId = league.Id
		t.ColorPrimary = v.Color1
		ret = append(ret, t)
	}

	//fmt.Printf("Found %d teams: %v \n", len(ret), ret)
	return ret, nil
}
