package cn

import (
	"appengine"
	"appengine/datastore"
	"errors"
	"github.com/chadkouse/hal"
)

var (
	ErrNoSuchTeam = errors.New("person not found")
)

type Team struct {
	Id           int64  `json:"id", datastore:"-"`
	Name         string `json:"name"`
	LeagueId     int64  `json:"leagueId"`
	Subreddit    string `json:"subreddit"`
	ColorPrimary string `json:"colorPrimary"`
}

func (t Team) GetMap() hal.Entry {
	return hal.Entry{
		"id":           t.Id,
		"name":         t.Name,
		"leageId":      t.LeagueId,
		"subreddit":    t.Subreddit,
		"colorPrimary": t.ColorPrimary,
	}
}

func (p *Team) GetKey(c appengine.Context) *datastore.Key {
	return datastore.NewKey(c, "Team", "", p.Id, nil)
}

func (p *Team) Create(c appengine.Context) (*datastore.Key, error) {

	pk := datastore.NewKey(c, "League", "", p.LeagueId, nil)
	k := datastore.NewIncompleteKey(c, "Team", pk)

	newKey, err := datastore.Put(c, k, p)
	p.Id = newKey.IntID()

	return newKey, err
}

func (p *Team) Get(c appengine.Context) error {

	k := datastore.NewKey(c, "Team", "", p.Id, nil)

	err := datastore.Get(c, k, &p)
	if err == datastore.ErrNoSuchEntity {
		err = ErrNoSuchTeam
	}

	if err != nil {
		return err
	}
	p.Id = k.IntID()

	return nil

}

func (p *Team) Update(c appengine.Context) (*datastore.Key, error) {

	k := p.GetKey(c)

	return datastore.Put(c, k, p)

}

func (p *Team) Delete(c appengine.Context) error {

	k := p.GetKey(c)
	return datastore.Delete(c, k)
}

func GetAllTeams(c appengine.Context, startKey string, limit int) (string, []Team, error) {

	q := datastore.NewQuery("Team")

	cursor, err := datastore.DecodeCursor(startKey)
	if err == nil {
		q = q.Start(cursor)
	}

	// Iterate over the results.
	teams := []Team{}

	if limit > 0 {
		q = q.Limit(limit)
	}

	t := q.Run(c)
	for {
		team := Team{}
		k, err := t.Next(&team)
		if err == datastore.Done {
			break
		}
		if err != nil {
			continue
		}

		team.Id = k.IntID()

		teams = append(teams, team)
	}

	// Get updated cursor and store it for next time.
	if cursor, err := t.Cursor(); err == nil {
		return cursor.String(), teams, err
	}

	return "", teams, err

}

func GetTeamByName(c appengine.Context, name string) (Team, error) {

	q := datastore.NewQuery("Team").Filter("Name =", name)

	// Iterate over the results.
	teams := []Team{}
	team := Team{}

	keys, err := q.GetAll(c, &teams)
	if err != nil {
		return team, err
	}

	for i := range keys {
		teams[i].Id = keys[i].IntID()
	}

	if len(teams) > 0 {
		team = teams[0]
	}
	return team, nil

}
