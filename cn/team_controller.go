package cn

import (
	"appengine"
	"encoding/json"
	"fmt"
	"github.com/chadkouse/hal"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"
)

func (t Team) GetSelfLink() string {
	return fmt.Sprintf("/team/%v", t.Id)
}

func CreateTeamHandler(rw http.ResponseWriter, req *http.Request) {
	decoder := json.NewDecoder(req.Body)
	l := new(Team)
	err := decoder.Decode(&l)
	if err != nil {
		fmt.Fprintf(rw, "Error %q", err)
		return
	}

	c := appengine.NewContext(req)

	_, err = l.Create(c)

	if err != nil {
		http.Error(rw, fmt.Sprintf("Error %v", err), http.StatusInternalServerError)
		return
	}

	fmt.Fprintf(rw, "Created new league %s", l)

}

func GetTeamHandler(rw http.ResponseWriter, req *http.Request) {

	vars := mux.Vars(req)
	id, err := strconv.ParseInt(vars["id"], 10, 64)

	if err != nil {
		http.Error(rw, fmt.Sprintf("Error parsing id %v", err), http.StatusInternalServerError)
		return
	}

	c := appengine.NewContext(req)

	l := Team{Id: id}
	err = l.Get(c)

	if err == ErrNoSuchTeam {
		http.Error(rw, err.Error(), http.StatusNotFound)
		return
	}

	if err != nil {
		http.Error(rw, fmt.Sprintf("Error getting league %v", err), http.StatusInternalServerError)
		return
	}

	rl := hal.NewResource(l, l.GetSelfLink())

	js, err := rl.MarshalJSON()
	if err != nil {
		http.Error(rw, err.Error(), http.StatusInternalServerError)
		return
	}

	rw.Header().Set("Content-Type", "application/json")
	rw.Write(js)
}

func UpdateTeamHandler(rw http.ResponseWriter, req *http.Request) {

	vars := mux.Vars(req)
	id, err := strconv.ParseInt(vars["id"], 10, 64)

	if err != nil {
		http.Error(rw, fmt.Sprintf("Error parsing id %v", err), http.StatusInternalServerError)
		return
	}

	c := appengine.NewContext(req)

	l := Team{Id: id}
	err = l.Get(c)

	if err != nil {
		http.Error(rw, fmt.Sprintf("Error getting league %v", err), http.StatusInternalServerError)
		return
	}

	//TODO update from post data

	_, err = l.Update(c)

	if err != nil {
		http.Error(rw, fmt.Sprintf("Error updating league %v", err), http.StatusInternalServerError)
		return
	}

	fmt.Fprintf(rw, "Updated league %s", l)

}

func DeleteTeamHandler(rw http.ResponseWriter, req *http.Request) {

	vars := mux.Vars(req)
	id, err := strconv.ParseInt(vars["id"], 10, 64)

	if err != nil {
		http.Error(rw, fmt.Sprintf("Error parsing id %v", err), http.StatusInternalServerError)
		return
	}

	c := appengine.NewContext(req)

	t := Team{Id: id}
	err = t.Delete(c)

	if err != nil {
		http.Error(rw, fmt.Sprintf("Error updating league %v", err), http.StatusInternalServerError)
		return
	}

	fmt.Fprintf(rw, "Deleted league %i", id)

}

func GetAllTeamHandler(rw http.ResponseWriter, req *http.Request) {

	startKey := req.FormValue("startKey")
	prevStartKey := req.FormValue("prevStartKey")
	limit, err := strconv.Atoi(req.FormValue("limit"))
	if err != nil {
		limit = 10
	}

	c := appengine.NewContext(req)

	newStartKey, leagues, err := GetAllTeams(c, startKey, limit)

	if err != nil {
		http.Error(rw, fmt.Sprintf("Error querying league %v", err), http.StatusInternalServerError)
		return
	}

	pr := PaginatedResource{
		SelfBase:     "/league",
		StartKey:     startKey,
		PrevStartKey: prevStartKey,
		NextStartKey: newStartKey,
		Limit:        limit,
	}

	pl := pr.GetResource()

	for _, lv := range leagues {
		pl.Embed("leagues", hal.NewResource(lv, lv.GetSelfLink()))
	}

	js, err := pl.MarshalJSON()
	if err != nil {
		http.Error(rw, err.Error(), http.StatusInternalServerError)
		return
	}

	rw.Header().Set("Content-Type", "application/json")
	rw.Write(js)

}
