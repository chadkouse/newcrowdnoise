package cn

import (
	"appengine"
	"encoding/json"
	"fmt"
	"github.com/chadkouse/hal"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"
)

func (l League) getSelfLink() string {
	return fmt.Sprintf("/league/%v", l.Id)
}

func CreateLeagueHandler(rw http.ResponseWriter, req *http.Request) {
	c := appengine.NewContext(req)
	c.Infof("body %q", req.Body)
	decoder := json.NewDecoder(req.Body)
	l := new(League)
	err := decoder.Decode(&l)
	if err != nil {
		c.Errorf("Error %v", err)
		fmt.Fprintf(rw, "Error %q", err)
		return
	}

	c.Infof("league %v", l)

	_, err = l.Create(c)

	if err != nil {
		http.Error(rw, fmt.Sprintf("Error %v", err), http.StatusInternalServerError)
		return
	}

	fmt.Fprintf(rw, "Created new league %s", l)

}

func GetLeagueHandler(rw http.ResponseWriter, req *http.Request) {

	vars := mux.Vars(req)
	id, err := strconv.ParseInt(vars["id"], 10, 64)

	if err != nil {
		http.Error(rw, fmt.Sprintf("Error parsing id %v", err), http.StatusInternalServerError)
		return
	}

	c := appengine.NewContext(req)

	l := League{Id: id}
	err = l.Get(c)

	if err == ErrNoSuchLeague {
		http.Error(rw, err.Error(), http.StatusNotFound)
		return
	}

	if err != nil {
		http.Error(rw, fmt.Sprintf("Error getting league %v", err), http.StatusInternalServerError)
		return
	}

	rl := hal.NewResource(l, l.getSelfLink())

	js, err := rl.MarshalJSON()
	if err != nil {
		http.Error(rw, err.Error(), http.StatusInternalServerError)
		return
	}

	rw.Header().Set("Content-Type", "application/json")
	rw.Write(js)
}

func GetLeagueTeamsHandler(rw http.ResponseWriter, req *http.Request) {
	startKey := req.FormValue("startKey")
	prevStartKey := req.FormValue("prevStartKey")
	limit, err := strconv.Atoi(req.FormValue("limit"))
	if err != nil {
		limit = 10
	}

	vars := mux.Vars(req)
	id, err := strconv.ParseInt(vars["id"], 10, 64)

	c := appengine.NewContext(req)

	newStartKey, teams, err := GetAllTeamsByLeagueId(c, id, startKey, limit)

	if err != nil {
		http.Error(rw, fmt.Sprintf("Error querying league %v", err), http.StatusInternalServerError)
		return
	}

	pr := PaginatedResource{
		SelfBase:     fmt.Sprintf("/leagues/%v/teams", id),
		StartKey:     startKey,
		PrevStartKey: prevStartKey,
		NextStartKey: newStartKey,
		Limit:        limit,
	}

	pl := pr.GetResource()

	for _, lv := range teams {
		pl.Embed("teams", hal.NewResource(lv, lv.GetSelfLink()))
	}

	js, err := pl.MarshalJSON()
	if err != nil {
		http.Error(rw, err.Error(), http.StatusInternalServerError)
		return
	}

	rw.Header().Set("Content-Type", "application/json")
	rw.Write(js)

}

func UpdateLeagueHandler(rw http.ResponseWriter, req *http.Request) {

	vars := mux.Vars(req)
	id, err := strconv.ParseInt(vars["id"], 10, 64)

	if err != nil {
		http.Error(rw, fmt.Sprintf("Error parsing id %v", err), http.StatusInternalServerError)
		return
	}

	c := appengine.NewContext(req)

	l := League{Id: id}
	err = l.Get(c)

	if err != nil {
		http.Error(rw, fmt.Sprintf("Error getting league %v", err), http.StatusInternalServerError)
		return
	}

	//TODO update from post data

	_, err = l.Update(c)

	if err != nil {
		http.Error(rw, fmt.Sprintf("Error updating league %v", err), http.StatusInternalServerError)
		return
	}

	fmt.Fprintf(rw, "Updated league %s", l)

}

func DeleteLeagueHandler(rw http.ResponseWriter, req *http.Request) {

	vars := mux.Vars(req)
	id, err := strconv.ParseInt(vars["id"], 10, 64)

	if err != nil {
		http.Error(rw, fmt.Sprintf("Error parsing id %v", err), http.StatusInternalServerError)
		return
	}

	c := appengine.NewContext(req)

	l := League{Id: id}
	err = l.Delete(c)

	if err != nil {
		http.Error(rw, fmt.Sprintf("Error updating league %v", err), http.StatusInternalServerError)
		return
	}

	fmt.Fprintf(rw, "Deleted league %i", id)

}

func GetAllLeagueHandler(rw http.ResponseWriter, req *http.Request) {

	startKey := req.FormValue("startKey")
	prevStartKey := req.FormValue("prevStartKey")
	limit, err := strconv.Atoi(req.FormValue("limit"))
	if err != nil {
		limit = 10
	}

	c := appengine.NewContext(req)

	newStartKey, leagues, err := GetAllLeagues(c, startKey, limit)

	if err != nil {
		http.Error(rw, fmt.Sprintf("Error querying league %v", err), http.StatusInternalServerError)
		return
	}

	pr := PaginatedResource{
		SelfBase:     "/league",
		StartKey:     startKey,
		PrevStartKey: prevStartKey,
		NextStartKey: newStartKey,
		Limit:        limit,
	}

	pl := pr.GetResource()

	for _, lv := range leagues {
		pl.Embed("leagues", hal.NewResource(lv, lv.getSelfLink()))
	}

	js, err := pl.MarshalJSON()
	if err != nil {
		http.Error(rw, err.Error(), http.StatusInternalServerError)
		return
	}

	rw.Header().Set("Content-Type", "application/json")
	rw.Write(js)

}
