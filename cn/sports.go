package cn

import (
	"appengine"
	"net/http"
	"time"
)

type SportsFetcher interface {
	FetchEvents(ctx appengine.Context, league League, start time.Time, end time.Time) ([]Event, error)
	FetchTeams(ctx appengine.Context, league League) ([]Team, error)
	SetClient(client *http.Client)
	GetClient() *http.Client
}

func NewSportsFetcher(client *http.Client) SportsFetcher {
	var f SportsFetcher
	f = &TheScoreFetcher{}
	f.SetClient(client)
	return f
}
