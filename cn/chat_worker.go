package cn

import (
	"appengine"
	"net/http"
)

const (
	CRAWL_SECS = 60 * 60 * 8
	N_THREADS  = 1
)

type ChatWorker struct {
	fetcher ChatFetcher
	client  *http.Client
}

func NewChatWorker(client *http.Client) *ChatWorker {
	c := &ChatWorker{}
	c.fetcher = NewRedditFetcher()
	c.client = client
	return c
}

func (cw *ChatWorker) ScrapeUpcomingChats(ctx appengine.Context, insert bool) ([]Chat, error) {

	// Get upcoming events
	events, err := GetUpcomingEvents(ctx, CRAWL_SECS)

	if err != nil {
		ctx.Errorf("Error getting upcoming events %v", err)
		return nil, err
	}
	ctx.Infof("Got %v events", len(events))

	c := make(chan []Chat)

	// for each event
	runningThreads := 0
	var added []Chat

	for _, event := range events {
		go cw.scrapeMissingChats(ctx, event, c)
		runningThreads++

		if runningThreads >= N_THREADS {
			incoming := <-c
			if incoming != nil {
				added = append(added, incoming...)
			}
			runningThreads--
		}
	}

	for runningThreads > 0 {

		incoming := <-c
		if incoming != nil {
			added = append(added, incoming...)
		}
		runningThreads--
	}

	if insert {
		for _, c := range added {
			c.Create(ctx)
		}
	}

	return added, nil
}

func (cw *ChatWorker) scrapeMissingChats(ctx appengine.Context, event Event, c chan []Chat) {

	var ret []Chat
	chats, err := GetChatsForEvent(ctx, event.Id)

	if err != nil {
		ctx.Errorf("Error getting chats for event %v, %v", event.Id, err)
		c <- nil
		return
	}
	ctx.Infof("found %v chats for event %v", len(chats), event.Id)

	needsLeague := true
	needsHome := true
	needsAway := true

	for _, c := range chats {
		if c.TeamId > 0 {
			if c.TeamId == event.AwayTeamId {
				needsAway = false
			}

			if c.TeamId == event.HomeTeamId {
				needsHome = false
			}
		} else {
			// TeamId is zero for league chats
			needsLeague = false
		}
	}

	if needsAway || needsHome || needsLeague {
		//cw.logger.Debugf("[worker] event id %d. Has %d chats, needs: (home: %v, away: %v, league: %v)", event.Id, len(chats), needsHome, needsAway, needsLeague)
	} else {
		//cw.logger.Debugf("[worker] event id %d has all chats already, skipping.", event.Id)
		c <- ret
		return
	}

	if needsAway {
		away := Team{Id: event.AwayTeamId}
		err := away.Get(ctx)
		if err == nil {
			chat, err := cw.fetcher.FetchTeamChat(cw.client, away, event)
			if err == nil {
				ret = append(ret, chat)
			}
		}
	}

	if needsHome {
		home := Team{Id: event.HomeTeamId}
		err := home.Get(ctx)
		if err == nil {
			chat, err := cw.fetcher.FetchTeamChat(cw.client, home, event)
			if err == nil {
				ret = append(ret, chat)
			}
		}

	}

	if needsLeague {
		league := League{Id: event.LeagueId}
		err := league.Get(ctx)
		if err == nil {
			chat, err := cw.fetcher.FetchLeagueChat(ctx, cw.client, league, event)
			if err == nil {
				ret = append(ret, chat)
			}
		}
	}

	c <- ret
}
