package cn

import (
	"appengine"
	"encoding/json"
	"fmt"
	"github.com/chadkouse/hal"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"
)

func (e Event) getSelfLink() string {
	return fmt.Sprintf("/event/%v", e.Id)
}

func CreateEventHandler(rw http.ResponseWriter, req *http.Request) {
	decoder := json.NewDecoder(req.Body)
	l := new(Event)
	err := decoder.Decode(&l)
	if err != nil {
		fmt.Fprintf(rw, "Error %q", err)
		return
	}

	c := appengine.NewContext(req)

	_, err = l.Create(c)

	if err != nil {
		http.Error(rw, fmt.Sprintf("Error %v", err), http.StatusInternalServerError)
		return
	}

	fmt.Fprintf(rw, "Created new chat %s", l)

}

func GetEventHandler(rw http.ResponseWriter, req *http.Request) {

	vars := mux.Vars(req)
	id, err := strconv.ParseInt(vars["id"], 10, 64)

	if err != nil {
		http.Error(rw, fmt.Sprintf("Error parsing id %v", err), http.StatusInternalServerError)
		return
	}

	c := appengine.NewContext(req)

	l := Event{Id: id}
	err = l.Get(c)

	if err == ErrNoSuchEvent {
		http.Error(rw, err.Error(), http.StatusNotFound)
		return
	}

	if err != nil {
		http.Error(rw, fmt.Sprintf("Error getting chat %v", err), http.StatusInternalServerError)
		return
	}

	rl := hal.NewResource(l, l.getSelfLink())

	js, err := rl.MarshalJSON()
	if err != nil {
		http.Error(rw, err.Error(), http.StatusInternalServerError)
		return
	}

	rw.Header().Set("Content-Type", "application/json")
	rw.Write(js)
}

func UpdateEventHandler(rw http.ResponseWriter, req *http.Request) {

	vars := mux.Vars(req)
	id, err := strconv.ParseInt(vars["id"], 10, 64)

	if err != nil {
		http.Error(rw, fmt.Sprintf("Error parsing id %v", err), http.StatusInternalServerError)
		return
	}

	c := appengine.NewContext(req)

	l := Event{Id: id}
	err = l.Get(c)

	if err != nil {
		http.Error(rw, fmt.Sprintf("Error getting chat %v", err), http.StatusInternalServerError)
		return
	}

	//TODO update from post data

	_, err = l.Update(c)

	if err != nil {
		http.Error(rw, fmt.Sprintf("Error updating chat %v", err), http.StatusInternalServerError)
		return
	}

	fmt.Fprintf(rw, "Updated chat %s", l)

}

func DeleteEventHandler(rw http.ResponseWriter, req *http.Request) {

	vars := mux.Vars(req)
	id, err := strconv.ParseInt(vars["id"], 10, 64)

	if err != nil {
		http.Error(rw, fmt.Sprintf("Error parsing id %v", err), http.StatusInternalServerError)
		return
	}

	c := appengine.NewContext(req)

	l := Event{Id: id}
	err = l.Delete(c)

	if err != nil {
		http.Error(rw, fmt.Sprintf("Error updating chat %v", err), http.StatusInternalServerError)
		return
	}

	fmt.Fprintf(rw, "Deleted chat %i", id)

}

func GetAllEventHandler(rw http.ResponseWriter, req *http.Request) {

	startKey := req.FormValue("startKey")
	prevStartKey := req.FormValue("prevStartKey")
	limit, err := strconv.Atoi(req.FormValue("limit"))
	if err != nil {
		limit = 10
	}

	c := appengine.NewContext(req)

	newStartKey, chats, err := GetAllEvents(c, startKey, limit)

	if err != nil {
		http.Error(rw, fmt.Sprintf("Error querying chat %v", err), http.StatusInternalServerError)
		return
	}

	pr := PaginatedResource{
		SelfBase:     "/chat",
		StartKey:     startKey,
		PrevStartKey: prevStartKey,
		NextStartKey: newStartKey,
		Limit:        limit,
	}

	pl := pr.GetResource()

	for _, lv := range chats {
		pl.Embed("chats", hal.NewResource(lv, lv.getSelfLink()))
	}

	js, err := pl.MarshalJSON()
	if err != nil {
		http.Error(rw, err.Error(), http.StatusInternalServerError)
		return
	}

	rw.Header().Set("Content-Type", "application/json")
	rw.Write(js)

}
