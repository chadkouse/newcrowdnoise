package cn

import (
	"net/http"

	"testing"
)

func TestGetTeamGameThread(t *testing.T) {

	r := NewRedditFetcher()
	team := Team{}
	team.Subreddit = "torontoraptors"

	event := Chat{}

	chat, err := r.FetchTeamChat(&http.Client{}, team, event)
	if err != nil {
		t.Error(err)
		return
	}

	t.Log("Team thread: " + chat.Value)

}

func TestGetLeagueThread(t *testing.T) {

	r := NewRedditFetcher()

	event := Event{}
	event.AwayTeamId = 23
	event.HomeTeamId = 7

	event2 := Event{}
	event2.AwayTeamId = 96
	event2.HomeTeamId = 75

	league := League{}
	league.Subreddit = "nfl"

	chat, err := r.FetchLeagueChat(&http.Client{}, league, event)
	if err != nil {
		t.Error(err)
		return
	}
	t.Log("League thread: " + chat.Value)

	chat, err = r.FetchLeagueChat(&http.Client{}, league, event2)
	if err != nil {
		t.Error(err)
		return
	}
	t.Log("League thread: " + chat.Value)

}

type RedditThreadTest struct {
	Name   string
	IsGame bool
	TeamA  string
	TeamB  string
}

var THREADS = []RedditThreadTest{
	RedditThreadTest{
		Name:   "Game Thread: #5 Predators (25-9-3) at #1 Ducks (25-9-6) - 01/04/2015 - 5:00 PT",
		IsGame: true,
		TeamA:  "Nashville Predators",
		TeamB:  "Anaheim Ducks"},

	RedditThreadTest{
		Name:   "Game Thread: #20 Stars (18-14-5) at #7 Blackhawks (25-11-2) - 01/04/2015 - 7:00 CT",
		IsGame: true,
		TeamA:  "Dallas Stars",
		TeamB:  "Chicago Blackhawks"},

	RedditThreadTest{
		Name:   "GAME THREAD: Toronto Raptors (24-9) @ Phoenix Suns (19-16) - (Jan. 04, 2015)",
		IsGame: true,
		TeamA:  "Toronto Raptors",
		TeamB:  "Phoenix Suns"},
	RedditThreadTest{
		Name:   "Game Thread: Detroit Lions (11-5) at Dallas Cowboys (12-4)",
		IsGame: true,
		TeamA:  "Detroit Lions",
		TeamB:  "Dallas Cowboys"},

	RedditThreadTest{
		Name:   "Stars v. Chicago - Pre-Game and Line Info",
		IsGame: false},

	RedditThreadTest{
		Name:   "5 Hawks among first 6 selected for NHL All-Star Game",
		IsGame: false},

	RedditThreadTest{
		Name:   "Bickel/Demers/Fiddler/everybody fight from game vs Wild",
		IsGame: false},

	RedditThreadTest{
		Name:   "2015 Winter Classic Interactive Panorama.",
		IsGame: false},

	RedditThreadTest{
		Name:   "Jaromir Jagr's first career hat-trick was on Feb-2, 1991; before teammates Eric Gelinas, Damon Severson, Adam Larsson, Jon Merrill and Jacob Josefson were born.",
		IsGame: false}}

func TestThreadRegex(t *testing.T) {

	r := &redditFetcher{}

	for _, v := range THREADS {
		if r.isGameThread(v.Name) != v.IsGame {
			t.Error("Failed game thread test: Expected", v.IsGame, ". Title:", v.Name)
			return
		}

		if len(v.TeamA) > 0 {
			if !r.isRelatedToTeam(v.Name, Team{Name: v.TeamB}) {
				t.Error("Should be related to", v.TeamA, ". Title: ", v.Name)
			}
		}

		if len(v.TeamB) > 0 {
			if !r.isRelatedToTeam(v.Name, Team{Name: v.TeamB}) {
				t.Error("Should be related to", v.TeamB, ". Title: ", v.Name)
			}
		}

	}
}
