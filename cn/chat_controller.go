package cn

import (
	"appengine"
	"appengine/urlfetch"
	"encoding/json"
	"fmt"
	"github.com/chadkouse/hal"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"
)

func (c Chat) getSelfLink() string {
	return fmt.Sprintf("/chat/%v", c.Id)
}

func CreateChatHandler(rw http.ResponseWriter, req *http.Request) {
	decoder := json.NewDecoder(req.Body)
	l := new(Chat)
	err := decoder.Decode(&l)
	if err != nil {
		fmt.Fprintf(rw, "Error %q", err)
		return
	}

	c := appengine.NewContext(req)

	_, err = l.Create(c)

	if err != nil {
		http.Error(rw, fmt.Sprintf("Error %v", err), http.StatusInternalServerError)
		return
	}

	fmt.Fprintf(rw, "Created new chat %s", l)

}

func GetChatHandler(rw http.ResponseWriter, req *http.Request) {

	vars := mux.Vars(req)
	id, err := strconv.ParseInt(vars["id"], 10, 64)

	if err != nil {
		http.Error(rw, fmt.Sprintf("Error parsing id %v", err), http.StatusInternalServerError)
		return
	}

	c := appengine.NewContext(req)

	l := Chat{Id: id}
	err = l.Get(c)

	if err == ErrNoSuchChat {
		http.Error(rw, err.Error(), http.StatusNotFound)
		return
	}

	if err != nil {
		http.Error(rw, fmt.Sprintf("Error getting chat %v", err), http.StatusInternalServerError)
		return
	}

	rl := hal.NewResource(l, l.getSelfLink())

	js, err := rl.MarshalJSON()
	if err != nil {
		http.Error(rw, err.Error(), http.StatusInternalServerError)
		return
	}

	rw.Header().Set("Content-Type", "application/json")
	rw.Write(js)
}

func UpdateChatHandler(rw http.ResponseWriter, req *http.Request) {

	vars := mux.Vars(req)
	id, err := strconv.ParseInt(vars["id"], 10, 64)

	if err != nil {
		http.Error(rw, fmt.Sprintf("Error parsing id %v", err), http.StatusInternalServerError)
		return
	}

	c := appengine.NewContext(req)
	l := Chat{Id: id}
	err = l.Get(c)

	if err != nil {
		http.Error(rw, fmt.Sprintf("Error getting chat %v", err), http.StatusInternalServerError)
		return
	}

	//TODO update from post data

	_, err = l.Update(c)

	if err != nil {
		http.Error(rw, fmt.Sprintf("Error updating chat %v", err), http.StatusInternalServerError)
		return
	}

	fmt.Fprintf(rw, "Updated chat %s", l)

}

func DeleteChatHandler(rw http.ResponseWriter, req *http.Request) {

	vars := mux.Vars(req)
	id, err := strconv.ParseInt(vars["id"], 10, 64)

	if err != nil {
		http.Error(rw, fmt.Sprintf("Error parsing id %v", err), http.StatusInternalServerError)
		return
	}

	c := appengine.NewContext(req)

	cd := Chat{Id: id}
	err = cd.Delete(c)

	if err != nil {
		http.Error(rw, fmt.Sprintf("Error updating chat %v", err), http.StatusInternalServerError)
		return
	}

	fmt.Fprintf(rw, "Deleted chat %i", id)

}

func GetAllChatHandler(rw http.ResponseWriter, req *http.Request) {

	startKey := req.FormValue("startKey")
	prevStartKey := req.FormValue("prevStartKey")
	limit, err := strconv.Atoi(req.FormValue("limit"))
	if err != nil {
		limit = 10
	}

	c := appengine.NewContext(req)

	newStartKey, chats, err := GetAllChats(c, startKey, limit)

	if err != nil {
		http.Error(rw, fmt.Sprintf("Error querying chat %v", err), http.StatusInternalServerError)
		return
	}

	p := PaginatedResource{
		SelfBase:     "/chat",
		StartKey:     startKey,
		PrevStartKey: prevStartKey,
		NextStartKey: newStartKey,
		Limit:        limit,
	}

	pr := p.GetResource()

	for _, lv := range chats {
		lr := hal.NewResource(lv, lv.getSelfLink())
		pr.Embed("chats", lr)
	}

	js, err := pr.MarshalJSON()
	if err != nil {
		http.Error(rw, err.Error(), http.StatusInternalServerError)
		return
	}

	rw.Header().Set("Content-Type", "application/json")
	rw.Write(js)

}

type ChatsResponse struct {
	AddedChats []Chat `json:"added"`
}

func RefreshChats(rw http.ResponseWriter, req *http.Request) {
	ctx := appengine.NewContext(req)

	cw := NewChatWorker(urlfetch.Client(ctx))

	res, err := cw.ScrapeUpcomingChats(ctx, true)

	if err != nil {
		ctx.Errorf("[worker] Refresh failed due to error: %v", err)
		http.Error(rw, err.Error(), 500)
		return
	}

	response, err := json.Marshal(ChatsResponse{AddedChats: res})
	if err != nil {
		http.Error(rw, err.Error(), 500)
		return
	}

	rw.Write(response)
}
