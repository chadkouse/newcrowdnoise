package cn

import (
	_ "github.com/go-sql-driver/mysql"
	"github.com/hobbs/GoReddit"

	"appengine"
)

const (
	API_ROUTE    = "/api/"
	ADMIN_ROUTE  = "/admin/"
	WORKER_ROUTE = "/worker/"

	DEV_DSN = "sportschat:sportschat@/sportschat?charset=utf8&parseTime=True"
	//DEV_DSN = "crowdnoise:crowdnoise@tcp(173.194.248.235:3306)/crowdnoise?charset=utf8&parseTime=True"
	PROD_DSN = "crowdnoise:crowdnoise@cloudsql(emerald-skill-726:sportschat)/crowdnoise?charset=utf8&parseTime=True"
)

func IsDev() bool {
	return appengine.IsDevAppServer()
}

func GetRedditClient() *goreddit.Client {
	return goreddit.NewClient("crowdnoise backend/user:zach978")
}
