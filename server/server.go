package server

import (
	"appengine"
	"appengine/taskqueue"
	"bitbucket.org/chadkouse/newcrowdnoise/cn"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"github.com/chadkouse/halgo"
	"github.com/gorilla/mux"
	"net/http"
	"os"
	"strconv"
)

func importLeagues(rw http.ResponseWriter, req *http.Request) {
	csvfile, err := os.Open("/Users/chadkouse/leagues.csv")

	if err != nil {
		fmt.Println(err)
		return
	}

	defer csvfile.Close()

	reader := csv.NewReader(csvfile)

	reader.FieldsPerRecord = -1 // see the Reader struct information below

	rawCSVdata, err := reader.ReadAll()

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	ctx := appengine.NewContext(req)

	for _, each := range rawCSVdata {
		league := new(cn.League)
		league.Name = each[1]
		league.Subreddit = each[2]
		i, _ := strconv.Atoi(each[3])
		league.IconChar = i
		league.IconColor = each[4]
		league.ShortName = each[5]

		jsonData, err := json.Marshal(league)
		if err != nil {
			continue
		}

		t := &taskqueue.Task{
			Path:    "/leagues",
			Method:  "POST",
			Payload: jsonData,
		}

		_, err = taskqueue.Add(ctx, t, "")
		if err == taskqueue.ErrTaskAlreadyAdded {
			err = nil
		}
		if err != nil {
			http.Error(rw, "Error importing", http.StatusInternalServerError)
		}
	}

}

func importTeams(rw http.ResponseWriter, req *http.Request) {
	csvfile, err := os.Open("/Users/chadkouse/teams.csv")

	if err != nil {
		fmt.Println(err)
		return
	}

	defer csvfile.Close()

	reader := csv.NewReader(csvfile)

	reader.FieldsPerRecord = -1 // see the Reader struct information below

	rawCSVdata, err := reader.ReadAll()

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	ctx := appengine.NewContext(req)

	for _, each := range rawCSVdata {
		team := new(cn.Team)
		team.Name = each[1]
		i, _ := strconv.ParseInt(each[2], 10, 64)
		switch i {
		case 1:
			team.LeagueId = 5655612935372800
			break
		case 2:
			team.LeagueId = 5092662981951488
			break
		case 3:
			team.LeagueId = 6218562888794112
			break
		case 4:
			team.LeagueId = 4811188005240832
			break
		default:
			team.LeagueId = i
			break
		}
		team.Subreddit = each[3]
		team.ColorPrimary = each[4]

		jsonData, err := json.Marshal(team)
		if err != nil {
			continue
		}

		t := &taskqueue.Task{
			Path:    "/teams",
			Method:  "POST",
			Payload: jsonData,
		}

		_, err = taskqueue.Add(ctx, t, "")
		if err == taskqueue.ErrTaskAlreadyAdded {
			err = nil
		}
		if err != nil {
			http.Error(rw, "Error importing", http.StatusInternalServerError)
		}
	}

}

func init() {

	r := mux.NewRouter()

	//	r.HandleFunc("/import", importTeams)
	//default handler
	r.HandleFunc("/", func(rw http.ResponseWriter, req *http.Request) {
		if req.Header.Get("Accept") == "application/hal+json" ||
			req.Header.Get("Accept") == "application/json" {

			result := halgo.Resource{
				Links: halgo.Links{}.
					Self("/").
					Add("de:events", halgo.Link{Href: "/api/events"}).
					Add("de:leagues", halgo.Link{Href: "/api/leagues"}).
					Add("de:teams", halgo.Link{Href: "/api/teams"}),
			}

			jsonData, err := json.Marshal(result)
			if err != nil {
				http.Error(rw, "Error parsing json", http.StatusInternalServerError)
				return
			}
			rw.Header().Add("content-type", "application/json")
			rw.Write(jsonData)
			return
		}

		http.ServeFile(rw, req, "./static/index.html")

	}).Methods("GET")

	//league handlers
	as := r.PathPrefix("/api").Subrouter()

	as.HandleFunc("/leagues", cn.CreateLeagueHandler).Methods("POST", "PUT")
	as.HandleFunc("/leagues", cn.GetAllLeagueHandler).Methods("GET")
	ls := as.PathPrefix("/leagues").Subrouter()
	ls.HandleFunc("/{id}", cn.GetLeagueHandler).Methods("GET")
	ls.HandleFunc("/{id}", cn.UpdateLeagueHandler).Methods("POST", "PUT")
	ls.HandleFunc("/{id}", cn.DeleteLeagueHandler).Methods("DELETE")
	ls.HandleFunc("/{id}/teams", cn.GetLeagueTeamsHandler).Methods("GET")
	ls.HandleFunc("/{id}/events", cn.GetLeagueHandler).Methods("GET")

	//team handlers
	as.HandleFunc("/teams", cn.CreateTeamHandler).Methods("POST", "PUT")
	as.HandleFunc("/teams", cn.GetAllTeamHandler).Methods("GET")
	ts := as.PathPrefix("/teams").Subrouter()
	ts.HandleFunc("/{id}", cn.GetTeamHandler).Methods("GET")
	ts.HandleFunc("/{id}", cn.UpdateTeamHandler).Methods("POST", "PUT")
	ts.HandleFunc("/{id}", cn.DeleteTeamHandler).Methods("DELETE")

	//event handlers
	as.HandleFunc("/events", cn.CreateEventHandler).Methods("POST", "PUT")
	as.HandleFunc("/events", cn.GetAllEventHandler).Methods("GET")
	es := as.PathPrefix("/events").Subrouter()
	es.HandleFunc("/{id}", cn.GetEventHandler).Methods("GET")
	es.HandleFunc("/{id}", cn.UpdateEventHandler).Methods("POST", "PUT")
	es.HandleFunc("/{id}", cn.DeleteEventHandler).Methods("DELETE")

	//chat handlers
	as.HandleFunc("/chats", cn.CreateChatHandler).Methods("POST", "PUT")
	as.HandleFunc("/chats", cn.GetAllChatHandler).Methods("GET")
	cs := as.PathPrefix("/chats").Subrouter()
	cs.HandleFunc("/{id}", cn.GetChatHandler).Methods("GET")
	cs.HandleFunc("/{id}", cn.UpdateChatHandler).Methods("POST", "PUT")
	cs.HandleFunc("/{id}", cn.DeleteChatHandler).Methods("DELETE")

	//task handlers
	taskSub := r.PathPrefix("/tasks").Subrouter()
	taskSub.HandleFunc("/refresh_chats", cn.RefreshChats).Methods("GET")
	//	taskSub.HandleFunc("/crawl/leagues", cn.CrawlAllLeagueTeams)
	//	taskSub.HandleFunc("/crawl/teams", cn.CrawlTeams)
	//	taskSub.HandleFunc("/crawl/sched", cn.CrawlSched)
	//	taskSub.HandleFunc("/crawl/subreddits", cn.CrawlSubreddits)

	http.Handle("/", r)
}
